function gameFieldTraversal(gameField) {
    const n = gameField.length;

    let diagSum = 0;
    let antiDiagSum = 0;

    // Наличие null в матрице говорит о том, что игра продолжается
    let isGameContinues = false;

    for (let i = 0; i < n; i++) {
        let rowSum = 0;
        let colSum = 0;
        for (let j = 0; j < n; j++) {

            // Обход строк
            if (gameField[i][j] === 'x') {
                rowSum++;
            } else if (gameField[i][j] === 'o') {
                rowSum--;
            } else {
                isGameContinues = true;
            }

            // Обход столбцов
            if (gameField[j][i] === 'x') {
                colSum++;
            } else if (gameField[j][i] === 'o') {
                colSum--;
            } else {
                isGameContinues = true;
            }

            // Обход главной диагонали
            if (i === j) {
                if (gameField[i][j] === 'x') {
                    diagSum++;
                } else if (gameField[i][j] === 'o') {
                    diagSum--;
                } else {
                    isGameContinues = true;
                }
            }

            // Обход побочной диагонали
            if (i == n - 1 - j) {
                if (gameField[i][j] === 'x') {
                    antiDiagSum++;
                } else if (gameField[i][j] === 'o') {
                    antiDiagSum--;
                } else {
                    isGameContinues = true;
                }
            }
            if (antiDiagSum === n) {
                return "Крестики выиграли";
            }
            if (antiDiagSum === -n) {
                return "Нолики выиграли";
            }
        }
        
        if (rowSum === n || colSum === n) {
            return "Крестики выиграли";
        }
        if (rowSum === -n || colSum === -n) {
            return "Нолики выиграли";
        }
    }

    if (diagSum === n) {
        return "Крестики выиграли";
    }
    if (diagSum === -n) {
        return "Нолики выиграли";
    }

    if (isGameContinues) {
        return "Игра продолжается";
    }
    
    return "Ничья";
}